JIRA integration test
=====================

This is a dummy repository to test JIRA integration from GitLab.

You find the corresponding JIRA project at https://jira.esss.lu.se/projects/TESTJIRA/
